import React from 'react';
import './employee.css';

function Employee() {
    return(
        <div className="employee">
            <h2>Employee Details</h2>
            <table>
                <tr>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>Designation</th>

                </tr>
                <tr>
                    <td>525</td>
                    <td>Manikandan</td>
                    <td>Consultant</td>
                </tr>
            </table>
        </div>

    );
}

export default Employee;