import React from 'react';
import './Partdetails.css';
import axios from 'axios';

class PartsItemDetails extends React.Component {
  viewMovie() {
    // console.log("Trying to view movie")
    // console.log(this.props.movie.title)
    const url = "https://www.themoviedb.org/movie/" + this.props.movie.key
    window.location.href = url
  }

  orderNow = () => {
    const partname = this.props.spareParts.partname;
    console.log("Order now clicked"+partname);
    axios.post('http://localhost:8080/autoparts/parts', {partname})
    .then(res =>{
      console.log(res.data);
    })

  } 

  render() {
    return <table id="row-item" key={this.props.spareParts.key}>
    <tbody>
      <tr>
        <td>
          <img alt="poster" width="120" src={this.props.spareParts.img} />
        </td>
        <td>
          <h3 id="titleName"><a class="title-click" href="#">{this.props.spareParts.partname}</a></h3>
          <h5>Price: <strong>{this.props.spareParts.price}</strong></h5>
          <p>{this.props.spareParts.description}</p>
          
          {/* <input type="button" onClick={this.viewMovie.bind(this)} value="View"/> */}
        </td>
        <td id="order-now-col">
          <button onClick={(event) => this.orderNow(this)}>Order Now</button>
        </td>
      </tr>
    </tbody>
  </table>
  }
}

export default PartsItemDetails;