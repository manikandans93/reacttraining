import React, {Component} from 'react';
import './Partdetails.css';
import PartsItemDetails from './PartdetailsItem';
import $ from 'jquery';
import axios from 'axios';

class Partdetails extends Component {
  
  state = {sparePartsList: []};

  componentWillMount() {
    axios.get('http://localhost:8080/autoparts').then((sparePartsList) => {
        console.log("sparePartsList", sparePartsList);
        this.setState({sparePartsList: sparePartsList.data})
    });
}
    constructor(props) {
       
         super(props)
        // this.performSearch()
    }

    performSearch = () => {
        
        const results = this.state.sparePartsList
        // console.log(results[0])

        var sparePartsRows = []

        // this.state.sparePartsList.map((spareParts, index) => )

        results.map((spareParts) => {
          const sparePartsRow = <PartsItemDetails spareParts={spareParts} key={spareParts.key} />
          console.log("SpareParts", sparePartsRow);
          sparePartsRows.push(sparePartsRow)
        }) 

        // results.forEach((spareParts) => {
        //   // spareParts.poster_src = "https://image.tmdb.org/t/p/w185" + movie.poster_path
        //   // console.log(movie.poster_path)
        //   const sparePartsRow = <MovieRow spareParts={spareParts} key={spareParts.key} />
        //   sparePartsRows.push(sparePartsRow)
        // })

        this.setState({rows: sparePartsRows})

      }
    
      render() {
        return (
          <div>
            {/* <input style={{
              fontSize: 24,
              display: 'block',
              width: "99%",
              paddingTop: 8,
              paddingBottom: 8,
              paddingLeft: 16
            }} onChange={this.searchChangeHandler} placeholder="Enter search term"/> */}
    
            {/* {this.state.rows} */}

            {this.state.sparePartsList.map(
                    (spareParts) => <PartsItemDetails spareParts={spareParts} key={spareParts.key} />
                )}
    
          </div>
        );
      }
    }

    export const searchChangeHandler = (event) => {
        console.log(event.target.value)
        const boundObject = this
        const searchTerm = event.target.value
        boundObject.performSearch(searchTerm)
      }
    
    export default Partdetails;
