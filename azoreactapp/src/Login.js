import React, {Component} from 'react';
import './Login.css';
import { Button } from 'react-bootstrap';

class Login extends Component {

    constructor() {
        super();
        
    }
    
    render() {
        
        return(
            <div className="login">
            
            <h1>Login Here</h1>
            <input id="inputbox" type="text" value={this.userName} 
            placeholder="Username" onChange={this.props.handleUserName} required /><br></br>
            <input id="inputbox" type="password" value={this.password} 
            placeholder="Password" onChange={this.props.handlePassword} required /><br></br>
            <Button type='submit' id="btn-submit" 
            onClick={this.props.handleSubmit}>Login</Button> 
            <label id="error_style">{this.props.error}</label>

            <p>Don't have an account? <a href="#" name="signup" onClick={this.props.handleSignupClick}>Sign Up</a></p>
            
            
        </div>
        );
    }
}

export default Login;