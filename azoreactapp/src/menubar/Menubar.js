import React from 'react';
import {searchChangeHandler} from '../partdetails/Partdetails';
import './menubar.css';

const Menubar =  ({onClick}) => {

    // logout = () => {
    //     console.log("logout clicked");
    //     this.setState({ isLoggedIn: false })
    // }

    // homeBtnClick = (event) => {
    //     console.log('Home Clicked');
    //     this.setState({component: event.target.name});
    // }

    // aboutUsBtnClick = (event) => {
    //     console.log('aboutus Clicked');
    //     this.setState({component: event.target.name});
    // }

    // contactUsBtnClick = (event) => {
    //     console.log('contactus Clicked'+event.target.name);
    //     this.setState({component: event.target.name});
        
    // }

    // render() {
        
    // }

    return(
        <div>
            <nav class="navbar navbar-expand-md navbar-dark bg-info">
                <a href="/" class="navbar-brand">AutoZone</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar4">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse collapse" id="navbar4">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#" name="home" 
                            onClick={onClick}>Home <span class="sr-only">Home</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/aboutus"
                            name="aboutus" onClick={onClick}>Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/cart"
                            name="cart" onClick={onClick}>Cart</a>
                        </li>
                    </ul>
                </div>
                <div></div>
                <form class="form-inline">
                    <div class="input-group">                    
                        <input type="text" class="form-control" placeholder="Search here" 
                        onChange= {searchChangeHandler} />
                        <div class="input-group-append">
                            <button type="button" class="btn btn-secondary">GO</button>
                        </div>
                    </div>
                </form>
                <div class="navbar-nav ml-auto">
                    <button id="btn-logout" >
                    <a href="/" class="nav-item nav-link" name='logout'> Logout</a>
                    </button>
                    
                </div>
            </nav>
            
        </div>
    );
}

export default Menubar;