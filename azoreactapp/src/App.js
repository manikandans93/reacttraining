import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Login';
import Home from './Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menubar from './menubar/Menubar';
import Header from './Header';
import Register from './Register';
import Cart from './Cart';
import Aboutus from './Aboutus';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoggedIn: false,
      isSignup: false,
      component: ''    
    }
    this.selectComponent = this.selectComponent.bind(this);
  }

  selectComponent(event){ 
    event.preventDefault();
    
    this.setState({component: event.target.name});
    }
    logout = () => {
      //     console.log("logout clicked");
          this.setState({ isLoggedIn: false })
      }
  render() {
    

        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header> 
        {this.state.isLoggedIn ? <Home /> : <Login />}
        */}

        let toRender = null;

        switch(this.state.component)
         {
           case 'home':
            console.log('component>>'+this.state.component);
           toRender = <Home />
          break;
           case 'aboutus':
           toRender = <Aboutus />
           break; 
           case 'cart':
           toRender = <Cart />
           break;

           case 'signup':
            this.setState({ isSignup: true })

          //  case 'logout' :
          //    logout = () => {
          //     this.setState({ isLoggedIn: false })
          //    };
           
          //  case default:
          //    toRender = <Home />
     
         }
     
        

        if(this.state.isLoggedIn) {
          return (
          <div className="App">
            <Menubar onClick = {this.selectComponent} />
            {toRender}
          </div>
          );
        } else {
          if(this.state.isSignup) {
            return(
              <div>
                <Register
                firstName={this.state.firstName}
                lastName = {this.state.lastName}
                email = {this.state.email}
                password = {this.state.password}
                error={this.state.error}
                handleFirstName = {(event) => this.setState({firstName: event.target.value})}
                handleLastName = {(event) => this.setState({lastName: event.target.value})}
                handleEmail = {(event) => this.setState({email: event.target.value})}
                handlePassword = {(event) => this.setState({password: event.target.value})}
                handleRegisterClick = {() => {
                  if(this.state.firstName !== '' 
                    && this.state.lastName !== ''
                    && this.state.email !== ''
                    && this.state.password !== '') {
                      this.setState({isSignup: false});
                    } else {
                      this.setState({error: 'please enter all fields'});
                    }
                }}/>
              </div>
            )
          } else {
            return(
              <div>
                <Login 
                  userName={this.state.userName}
                  password={this.state.password}
                  error={this.state.error}
                  handleUserName = {(event) => this.setState({userName: event.target.value})}
                  handlePassword = {(event) => this.setState({password: event.target.value})}
                  handleSubmit= {() => {
                    console.log('Button CLicked');
                    if(this.state.userName === 'user' 
                    && this.state.password === 'pass'){
                      this.setState({ isLoggedIn: true })
                      this.setState({component: 'home'})
                    } else {
                      this.setState({error: 'Invalid User'});
                    }
                    
                  }
                } 
                handleSignupClick=  {() => {
                  console.log('Signup Button CLicked');
                  this.setState({ isSignup: true })
                }
                }
                />
              </div>
            );
          }
          
          }
        
  }
  
}

export default App;
