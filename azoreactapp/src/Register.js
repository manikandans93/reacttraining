import React, { Component } from "react";
import './Register.css';

class Register extends Component {
    render() {
        return (
            <form>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>First name</label>
                    <input id="inputbox" type="text" className="form-control" onChange={this.props.handleFirstName} value={this.firstName} placeholder="First name" />
                </div>

                <div className="form-group">
                    <label>Last name</label>
                    <input id="inputbox" type="text" className="form-control" onChange={this.props.handleLastName} value={this.lastName} placeholder="Last name" />
                </div>

                <div className="form-group">
                    <label>Email address</label>
                    <input id="inputbox" type="email" className="form-control" onChange={this.props.handlemail} value={this.email} placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input id="inputbox" type="password" className="form-control" onChange={this.props.handlepassword} value={this.password} placeholder="Enter password" />
                </div>

                <button type="submit" id="inputbox" className="btn btn-primary btn-block" onClick={this.props.handleRegisterClick}>Sign Up</button>
                <p className="form-group">
                    Already registered <a href="/">sign in?</a>
                </p>
                <label id="error_style">{this.props.error}</label>
            </form>
        );
    }
} 
export default Register;


// import React, { useState } from "react";
// import { useHistory } from "react-router-dom";
// import {
//   HelpBlock,
//   FormGroup,
//   FormControl,
//   FormLabel
// } from "react-bootstrap";

// import { useAppContext } from './libs/contextLib';
// import { useFormFields } from './libs/hooksLib';
// import { onError } from './libs/errorLib';
// import './Register.css';

// export default function Signup() {
//   const [fields, handleFieldChange] = useFormFields({
//     email: "",
//     password: "",
//     confirmPassword: "",
//     confirmationCode: "",
//   });
//   const history = useHistory();
//   const [newUser, setNewUser] = useState(null);
//   const { userHasAuthenticated } = useAppContext();
//   const [isLoading, setIsLoading] = useState(false);

//   function validateForm() {
//     return (
//       fields.email.length > 0 &&
//       fields.password.length > 0 &&
//       fields.password === fields.confirmPassword
//     );
//   }

//   function validateConfirmationForm() {
//     return fields.confirmationCode.length > 0;
//   }

//   async function handleSubmit(event) {
//     event.preventDefault();

//     setIsLoading(true);

//     setNewUser("test");

//     setIsLoading(false);
//   }

//   async function handleConfirmationSubmit(event) {
//     event.preventDefault();

//     setIsLoading(true);
//   }

//   function renderConfirmationForm() {
//     return (
//       <form onSubmit={handleConfirmationSubmit}>
//         <FormGroup controlId="confirmationCode" bsSize="large">
//           <FormLabel>Confirmation Code</FormLabel>
//           <FormControl
//             autoFocus
//             type="tel"
//             onChange={handleFieldChange}
//             value={fields.confirmationCode}
//           />
          
//         </FormGroup>
        
//       </form>
//     );
//   }

//   function renderForm() {
//     return (
//       <form onSubmit={handleSubmit}>
//         <FormGroup controlId="email" bsSize="large">
//           <FormLabel>Email</FormLabel>
//           <FormControl
//             autoFocus
//             type="email"
//             value={fields.email}
//             onChange={handleFieldChange}
//           />
//         </FormGroup>
//         <FormGroup controlId="password" bsSize="large">
//           <FormLabel>Password</FormLabel>
//           <FormControl
//             type="password"
//             value={fields.password}
//             onChange={handleFieldChange}
//           />
//         </FormGroup>
//         <FormGroup controlId="confirmPassword" bsSize="large">
//           <FormLabel>Confirm Password</FormLabel>
//           <FormControl
//             type="password"
//             onChange={handleFieldChange}
//             value={fields.confirmPassword}
//           />
//         </FormGroup>
        
//       </form>
//     );
//   }

//   return (
//     <div className="Signup">
//       {newUser === null ? renderForm() : renderConfirmationForm()}
//     </div>
//   );
// }