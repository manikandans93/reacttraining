import React, {Component} from 'react';
import './App.css';

class Aboutus extends Component {
    render() {
        return(
            <div>
                <h2>Contact Us</h2>
                <p class="contact-body">
                We want your shopping experience with AutoZone.com to be easy 
                and enjoyable. For quick answers to frequently asked questions, 
                visit our  FAQs  section. For all other questions or comments, 
                please contact us on below e-mail.
                </p>
                
            </div>
        );
    }
}

export default Aboutus;