import React, {Component} from 'react';
import Partdetails from './partdetails/Partdetails';
import Header from './Header';

class Home extends Component {
    render() {
        return(
            <div>
                <Header />
                <Partdetails />
            </div>
        );
    }
}

export default Home;